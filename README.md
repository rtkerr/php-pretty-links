# PHP Pretty Links

#### What is this?

Just a simple router for static pages using php

#### Add a page

`~/app/Core/Router.php`

```php
<?php

// name the function the using your page name e.g Contact, About, Location

public function Page()
{

    // reference the page exluding the extension
    
    $this->method = $this->page('Page');

}
```

`/app/Pages/Page.php`

```php
<?php
require __PARTIALS__ . 'Head.php';
require __PARTIALS__ . 'Nav.php';
?>

<!--
include all of the HTML here
for easy referencing you can link the partials to make site wide changes
-->

<?php
require __PARTIALS__ . 'Foot.php';

```

#### Edit styling

`~/public/assets/csss/global.css`

```css
/* css document or include your own */
body {
color: #f5f5f5;
}
```