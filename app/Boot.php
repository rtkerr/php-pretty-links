<?php

/*
[ ERROR_display ] displays error for development. Remove before production (live application)
*/
ini_set('display_errors', 1);

/*
[ Config.php ] require in the configuration document
*/
require 'Config.php';

/*
[ Controller.php ] require in the router class document
*/
require __BASE__ . '/Core/Router.php';

/*
[ App.php ] require in the application class document
*/
require __BASE__ . '/Core/App.php';
