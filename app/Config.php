<?php
/*
[ -CONSTANTS ] define the base constants for easy referencing
*/

/*
[ __BASE__ ] the top directory of the application core
*/
define('__BASE__', __DIR__);

/*
[ __PAGES__ ] pages directory
*/
define('__PAGES__', __BASE__ . '/Pages/');

/*
[ __PARTIALS__ ] html partials to include.
*/
define('__PARTIALS__', __PAGES__ . '/Partials/');
