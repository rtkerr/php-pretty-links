<?php

class App
{

  protected $method = 'Index';

  protected $uri;

  /*
  [ f-__construct() ] grab the uri
  ] change case to lower then the first character to upper
  ] instantiate router class
  ] check to see if the page and method exists
  ] if they exist set the var to the uri
  ] call in the router function to load the page
  */
  public function __construct()
  {

    $uri = $this->get_uri();

    $uri_1 = ucfirst(strtolower($uri[1]));

    $Router = new Router();

    if ( file_exists( __PAGES__ . $uri_1 . '.php' ) && method_exists($Router, $uri_1))
    {

      $this->method = $uri_1;

    }

     call_user_func([$Router, $this->method]);

  }

  /*
  [ f-get_uri() ] parse and sanitize the requested uri
  */
  public function get_uri()
  {

    if ( isset( $_SERVER['REQUEST_URI'] ) )
    {
      return $uri = explode('/', filter_var($_SERVER['REQUEST_URI'], FILTER_SANITIZE_URL));
    }

  }

}
