<?php

class Router
{

  /*
  [ f-Index() ] base index page
  */
  public function Index()
  {
    $this->method = $this->page('Index');
  }

  /*
  [ f-Samplepage() ] samplepage page
  */
  public function Samplepage()
  {
    $this->method = $this->page('Samplepage');
  }

  /*
  [ f-page() ] bring in the pages
  */
  public function page($page)
  {
    require __PAGES__ . $page . '.php';
  }

}
