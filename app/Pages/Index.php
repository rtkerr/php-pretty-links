<?php
/*
[ Head.php ] require in the head partial document
*/
require __PARTIALS__ . 'Head.php';

/*
[ Nav.php ] require in the nav partial document
*/
require __PARTIALS__ . 'Nav.php';
?>

<!--
HTML goes here
-->

<h1 class="title-lrg" id="space">php pretty links</h1>
<h2 class="title-med" id="magic">very simple php router</h2>

<a href="./Samplepage">[ go to sample page ]</a>

<?php
/*
[ Foot.php ] require in the foot partial document
*/
require __PARTIALS__ . 'Foot.php';
