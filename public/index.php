<?php

/*
[ Boot.php ] require in the applications boot document
*/
require '../app/Boot.php';

/*
[ App() ] instantiate the application class
*/
$App = new App();
